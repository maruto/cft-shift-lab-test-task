package com.example.shiftlabtesttaskandroid.Navigator;

public interface INavigator {
    void showStartScreen();
    void showLoginScreen();
    void showRegistrationScreen();
    void showMainScreen();
}



