package com.example.shiftlabtesttaskandroid;

import android.app.Application;

import com.example.shiftlabtesttaskandroid.model.UserService;
import com.example.shiftlabtesttaskandroid.model.Validator.Validator;
import com.example.shiftlabtesttaskandroid.model.repository.Repository;

public class App extends Application {
    public UserService userService = new UserService(new Repository(), new Validator());;

    @Override
    public void onCreate() {
        super.onCreate();
    }
}

