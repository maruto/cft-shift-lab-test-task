package com.example.shiftlabtesttaskandroid;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.example.shiftlabtesttaskandroid.Navigator.INavigator;
import com.example.shiftlabtesttaskandroid.screens.loginScreen.LoginFragment;
import com.example.shiftlabtesttaskandroid.screens.mainScreen.MainScreenFragment;
import com.example.shiftlabtesttaskandroid.screens.registrationScreen.RegistrationFragment;
import com.example.shiftlabtesttaskandroid.screens.startScreen.StartFragment;


public class MainActivity extends FragmentActivity implements INavigator {

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragmentContainer, StartFragment.class, null)
                    .commit();
        }
    }


    private void launchFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commit();
    }

    public void showStartScreen() {
        launchFragment(new StartFragment());
    }

    @Override
    public void showLoginScreen() {
        launchFragment(new LoginFragment());
    }

    @Override
    public void showRegistrationScreen() {
        launchFragment(new RegistrationFragment());
    }

    @Override
    public void showMainScreen() {
        launchFragment(new MainScreenFragment());
    }
}
