package com.example.shiftlabtesttaskandroid.model;

public interface IUsersListener {
    void invoke(UserService newUserService);
}
