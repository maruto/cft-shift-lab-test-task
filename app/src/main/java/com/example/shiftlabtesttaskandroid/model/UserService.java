package com.example.shiftlabtesttaskandroid.model;

import com.example.shiftlabtesttaskandroid.model.Validator.IValidator;
import com.example.shiftlabtesttaskandroid.model.exceptions.UserAlreadyRegisteredException;
import com.example.shiftlabtesttaskandroid.model.exceptions.UserNotRegisteredException;
import com.example.shiftlabtesttaskandroid.model.exceptions.WrongPasswordException;
import com.example.shiftlabtesttaskandroid.model.repository.IRepository;

import java.util.ArrayList;

public class UserService {
    private User currentUser = null;
    private final ArrayList<IUsersListener> usersListeners;

    private final IRepository repository;
    private final IValidator validator;

    public UserService(IRepository repository, IValidator validator) {
        this.usersListeners = new ArrayList<>();
        this.repository = repository;
        this.validator = validator;
    }

    public boolean isCurrentUserSet(){
        return (currentUser != null);
    }

    public void registerNewUser(User user) throws UserAlreadyRegisteredException {
        repository.addUser(user);
        currentUser = user;
        notifyListeners();
    }

    public void loginUser(String login, String password) throws UserNotRegisteredException, WrongPasswordException {
        User user = repository.getUser(login);
        if(user.getPassword().compareTo(password) == 0) {
            currentUser = user;
            notifyListeners();
        } else
            throw new WrongPasswordException();
    }

    public boolean isUserRegistered(User user){
        return repository.isUserRegistered(user);
    }

    public void signOutUser( ) {
        currentUser = null;
        notifyListeners();
    }

    public User getCurrentUser(){
        return currentUser;
    }

    public IValidator getValidator() {
        return validator;
    }

    public void addListener(IUsersListener usersListener) {
        usersListeners.add(usersListener);
    }
    public void removeListener(IUsersListener usersListener) {
        usersListeners.remove(usersListener);
    }
    private void notifyListeners() {
        for(IUsersListener usersListener : usersListeners)
            usersListener.invoke(this);
    }
}

