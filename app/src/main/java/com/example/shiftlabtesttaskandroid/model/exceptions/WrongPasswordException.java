package com.example.shiftlabtesttaskandroid.model.exceptions;

public class WrongPasswordException extends MyAppException {
    public WrongPasswordException() {
        super("Wrong password");
    }
}
