package com.example.shiftlabtesttaskandroid.model;

public class User {
    private String name;
    private String surname;
    private String login;
    private String password;
    private int birthdayDay;
    private int birthdayMonth;
    private int birthdayYear;

    public User(String name, String surname, String login, String password, int birthdayDay, int birthdayMonth, int birthdayYear) {
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.birthdayDay = birthdayDay;
        this.birthdayMonth = birthdayMonth;
        this.birthdayYear = birthdayYear;
    }

    public String getName() {
        return name;
    }
    public String getLogin() {
        return login;
    }
    public String getSurname() {
        return surname;
    }
    public String getPassword() {
        return password;
    }
    public int getBirthdayDay() {
        return birthdayDay;
    }
    public int getBirthdayMonth() {
        return birthdayMonth;
    }
    public int getBirthdayYear() {
        return birthdayYear;
    }
}