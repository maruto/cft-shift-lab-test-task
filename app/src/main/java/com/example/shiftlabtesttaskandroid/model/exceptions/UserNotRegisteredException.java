package com.example.shiftlabtesttaskandroid.model.exceptions;

public class UserNotRegisteredException extends MyAppException {
    public UserNotRegisteredException() {
        super("User is not registered");
    }
}