package com.example.shiftlabtesttaskandroid.model.Validator;

public interface IValidator {
    boolean isNameValid(String name);
    boolean isSurnameValid(String surname);
    boolean isLoginValid(String login);
    boolean isPasswordValid(String password);
}
