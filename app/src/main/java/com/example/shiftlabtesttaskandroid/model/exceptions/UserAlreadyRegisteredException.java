package com.example.shiftlabtesttaskandroid.model.exceptions;

public class UserAlreadyRegisteredException extends MyAppException {
    public UserAlreadyRegisteredException() {
        super("User with this login is already registered");
    }
}

