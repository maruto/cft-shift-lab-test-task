package com.example.shiftlabtesttaskandroid.model.Validator;

public class Validator implements IValidator {
    @Override
    public boolean isNameValid(String name) {
        if(name.length() < 2)
            return false;
        char[] nameChar = name.toCharArray();
        for(char c : nameChar) {
            if(!Character.isLetter(c))
                return false;
        }
        return true;
    }

    @Override
    public boolean isSurnameValid(String surname) {
        if(surname.length() < 2)
            return false;
        char[] nameChar = surname.toCharArray();
        for(char c : nameChar) {
            if(!Character.isLetter(c))
                return false;
        }
        return true;
    }

    @Override
    public boolean isLoginValid(String login) {
        if(login.length() < 4)
            return false;
        char[] nameChar = login.toCharArray();
        for(char c : nameChar) {
            if(!(Character.isDefined(c) && Character.isLetterOrDigit(c)  && !Character.isWhitespace(c)))
                return false;
        }
        return true;
    }

    @Override
    public boolean isPasswordValid(String password) {
        int countUpperCase = 0;
        int countLowCase = 0;
        int countDigit = 0;

        if(password.length() < 8)
            return false;

        char[] nameChar = password.toCharArray();
        for(char c : nameChar) {
            if(!(Character.isDefined(c) && Character.isLetterOrDigit(c)  && !Character.isWhitespace(c)))
                return false;
            if(Character.isUpperCase(c)){
                countUpperCase++;
            } else if(Character.isLowerCase(c)) {
                countLowCase++;
            } else if(Character.isDigit(c)) {
                countDigit++;
            }
         }

        if(countDigit == 0)
            return false;
        if(countLowCase == 0)
            return false;
        if(countUpperCase == 0)
            return false;

        return true;
    }
}
