package com.example.shiftlabtesttaskandroid.model.exceptions;

public class MyAppException extends Exception {
    public MyAppException(String message) {
        super(message);
    }
}
