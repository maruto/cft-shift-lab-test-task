package com.example.shiftlabtesttaskandroid.model.exceptions;

public class DataNotValidException extends MyAppException {
    public DataNotValidException(String message) {
        super(message);
    }
}
