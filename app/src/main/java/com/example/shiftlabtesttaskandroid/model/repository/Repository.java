package com.example.shiftlabtesttaskandroid.model.repository;

import com.example.shiftlabtesttaskandroid.model.User;
import com.example.shiftlabtesttaskandroid.model.exceptions.UserAlreadyRegisteredException;
import com.example.shiftlabtesttaskandroid.model.exceptions.UserNotRegisteredException;

import java.util.HashMap;

public class Repository implements IRepository {
    private HashMap<String, User> users;

    public Repository() { users = new HashMap<>(); }

    @Override
    public void addUser(User user) throws UserAlreadyRegisteredException {
        if(!isUserRegistered(user))
            users.put(user.getLogin(), user);
        else
            throw new UserAlreadyRegisteredException();
    }

    @Override
    public User getUser(String login) throws UserNotRegisteredException {
        User user = users.get(login);
        if(user == null)
            throw new UserNotRegisteredException();
        else
            return user;
    }

    @Override
    public boolean isUserRegistered(User user) {
        return users.containsKey(user.getLogin());
    }

}
