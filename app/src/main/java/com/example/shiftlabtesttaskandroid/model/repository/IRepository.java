package com.example.shiftlabtesttaskandroid.model.repository;

import com.example.shiftlabtesttaskandroid.model.User;
import com.example.shiftlabtesttaskandroid.model.exceptions.*;

public interface IRepository {
    void addUser(User user) throws UserAlreadyRegisteredException;
    User getUser(String login) throws UserNotRegisteredException;
    boolean isUserRegistered(User user);
}
