package com.example.shiftlabtesttaskandroid.screens.loginScreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.shiftlabtesttaskandroid.App;
import com.example.shiftlabtesttaskandroid.Navigator.INavigator;
import com.example.shiftlabtesttaskandroid.R;
import com.example.shiftlabtesttaskandroid.model.exceptions.UserNotRegisteredException;
import com.example.shiftlabtesttaskandroid.model.exceptions.WrongPasswordException;
import com.example.shiftlabtesttaskandroid.screens.CustomTextWatcher;
import com.example.shiftlabtesttaskandroid.screens.ViewModelFactory;

public class LoginFragment extends Fragment {

    private LoginScreenViewModel viewModel;

    private Button loginButton;
    private EditText et_login;
    private EditText et_password;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                getNavigator().showStartScreen();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        viewModel = new ViewModelProvider(this, new ViewModelFactory((App) getActivity().getApplicationContext())).get(LoginScreenViewModel.class);

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        loginButton = view.findViewById(R.id.button_Login_log);
        et_login = view.findViewById(R.id.editText_login_log);
        et_password = view.findViewById(R.id.editText_password_log);
        loginButton.setEnabled(false);

        EditText[] edList = {et_login, et_password};
        CustomTextWatcher textWatcher = new CustomTextWatcher(edList, loginButton);
        for (EditText editText : edList) editText.addTextChangedListener(textWatcher);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.login.setValue(et_login.getText().toString());
                viewModel.password.setValue(et_password.getText().toString());

                try {
                    viewModel.loginUser();
                } catch (UserNotRegisteredException | WrongPasswordException e) {
                    Toast toast = Toast.makeText(getContext(), e.getMessage(),Toast.LENGTH_LONG);
                    toast.show();
                }
                if(viewModel.isDataValid.getValue())
                    getNavigator().showMainScreen();
            }
        });
    }

    private INavigator getNavigator() {
        INavigator navigator;
        FragmentActivity fragmentActivity = requireActivity();
        if(INavigator.class.isAssignableFrom(fragmentActivity.getClass())) {
            navigator = (INavigator) fragmentActivity;
        } else {
            throw new RuntimeException("FragmentActivity dosen't implement INavigator");
        }
        return navigator;
    }
}
