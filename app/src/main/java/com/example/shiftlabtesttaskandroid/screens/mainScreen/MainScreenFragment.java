package com.example.shiftlabtesttaskandroid.screens.mainScreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;

import com.example.shiftlabtesttaskandroid.App;
import com.example.shiftlabtesttaskandroid.Navigator.INavigator;
import com.example.shiftlabtesttaskandroid.R;
import com.example.shiftlabtesttaskandroid.model.User;
import com.example.shiftlabtesttaskandroid.screens.ViewModelFactory;

public class MainScreenFragment extends Fragment {
    private MainScreenViewModel viewModel;

    private Button singOutButton;
    private Button getUserInfoButton;
    private UserInfoDialogFragment myDialogFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        viewModel = new ViewModelProvider(this, new ViewModelFactory((App) getActivity().getApplicationContext())).get(MainScreenViewModel.class);

        View view = inflater.inflate(R.layout.fragment_main_screen, container, false);
        singOutButton = view.findViewById(R.id.button_signOut);
        getUserInfoButton = view.findViewById(R.id.button_getUserInfo);

        User currentUser = viewModel.currentUser.getValue();

        FragmentManager manager = getParentFragmentManager();
        myDialogFragment = new UserInfoDialogFragment(currentUser.getLogin(), getStringUserData(currentUser));

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        singOutButton.setOnClickListener(v -> {
            viewModel.signOut();
            getNavigator().showStartScreen();
        });
        getUserInfoButton.setOnClickListener(v -> {
            myDialogFragment.show(getParentFragmentManager(), "myDialog");
        });
    }

    private String getStringUserData(User user) {
        return user.getName() + " " + user.getSurname() + "\nDate of Birth: " + getDayFormat(user.getBirthdayDay()) + "."
                + user.getBirthdayMonth() + "." + user.getBirthdayYear();
    }

    private String getDayFormat(int day) {
        if (0 < day && day < 10) return "0" + day;
        else return String.valueOf(day);
    }

    private INavigator getNavigator() {
        INavigator navigator;
        FragmentActivity fragmentActivity = requireActivity();
        if (INavigator.class.isAssignableFrom(fragmentActivity.getClass())) {
            navigator = (INavigator) fragmentActivity;
        } else {
            throw new RuntimeException("FragmentActivity dosen't implement INavigator");
        }
        return navigator;
    }
}