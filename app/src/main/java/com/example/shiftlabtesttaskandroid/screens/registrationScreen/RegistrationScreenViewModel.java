package com.example.shiftlabtesttaskandroid.screens.registrationScreen;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.shiftlabtesttaskandroid.model.User;
import com.example.shiftlabtesttaskandroid.model.UserService;
import com.example.shiftlabtesttaskandroid.model.exceptions.DataNotValidException;
import com.example.shiftlabtesttaskandroid.model.exceptions.UserAlreadyRegisteredException;

public class RegistrationScreenViewModel extends ViewModel {
    private UserService userService;


    public MutableLiveData<String> name = new MutableLiveData<>();
    public MutableLiveData<String> surname = new MutableLiveData<>();

    public MutableLiveData<Integer> birthDay = new MutableLiveData<>();
    public MutableLiveData<Integer> birthMonth = new MutableLiveData<>();
    public MutableLiveData<Integer> birthYear = new MutableLiveData<>();

    public MutableLiveData<String> login = new MutableLiveData<>();
    public MutableLiveData<String> password = new MutableLiveData<>();
    public MutableLiveData<String> confPassword = new MutableLiveData<>();

    private MutableLiveData<Boolean> _isDataValid = new MutableLiveData<>();
    public LiveData<Boolean> isDataValid = _isDataValid;


    public RegistrationScreenViewModel(UserService userService) {
        this.userService = userService;
        _isDataValid.setValue(false);
    }

     public void registerNewUser() throws DataNotValidException, UserAlreadyRegisteredException {
         _isDataValid.setValue(false);

        if (!userService.getValidator().isNameValid(name.getValue())) {
             throw new DataNotValidException("Name must be at least 2 characters long and consist of letters");
         } else if (!userService.getValidator().isSurnameValid(surname.getValue())) {
             throw new DataNotValidException("Surname must be at least 2 characters long and consist of letters");
         } else if (!userService.getValidator().isLoginValid(login.getValue())) {
             throw new DataNotValidException("Login must be longer than 5 characters and consist of letters and numbers");
         } else if (!userService.getValidator().isPasswordValid(password.getValue())) {
             throw new DataNotValidException("Password must be at least 8 characters long, contain upper and lower case letters and numbers");
         } else if (password.getValue().compareTo(confPassword.getValue()) != 0) {
             throw new DataNotValidException("Passwords must match");
         }

         User newUser = new User(name.getValue(), surname.getValue(), login.getValue(), password.getValue(), birthDay.getValue(), birthMonth.getValue(), birthYear.getValue());

         userService.registerNewUser(newUser);
         _isDataValid.setValue(true);
    }
}
