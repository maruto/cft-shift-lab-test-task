package com.example.shiftlabtesttaskandroid.screens.registrationScreen;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.shiftlabtesttaskandroid.App;
import com.example.shiftlabtesttaskandroid.Navigator.INavigator;
import com.example.shiftlabtesttaskandroid.R;
import com.example.shiftlabtesttaskandroid.model.exceptions.DataNotValidException;
import com.example.shiftlabtesttaskandroid.model.exceptions.UserAlreadyRegisteredException;
import com.example.shiftlabtesttaskandroid.screens.CustomTextWatcher;
import com.example.shiftlabtesttaskandroid.screens.ViewModelFactory;

import java.util.Calendar;

public class RegistrationFragment extends Fragment {

    private RegistrationScreenViewModel viewModel;

    private Button dateButton;
    private Button registrationButton;
    private DatePickerDialog datePickerDialog;
    private EditText et_name;
    private EditText et_surname;
    private EditText et_login;
    private EditText et_password;
    private EditText et_confPassword;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                getNavigator().showStartScreen();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        viewModel = new ViewModelProvider(this, new ViewModelFactory((App) getActivity().getApplicationContext())).get(RegistrationScreenViewModel.class);


        View view = inflater.inflate(R.layout.fragment_registration, container, false);


        et_name = view.findViewById(R.id.editText_name_reg);
        et_surname = view.findViewById(R.id.editText_surname_reg);
        et_login = view.findViewById(R.id.editText_login_reg);
        et_password = view.findViewById(R.id.editText_password_reg);
        et_confPassword = view.findViewById(R.id.editText_confimPassword_reg);

        registrationButton = view.findViewById(R.id.button_registration_reg);
        registrationButton.setEnabled(false);


        EditText[] edList = {et_name, et_surname, et_login, et_password, et_confPassword};
        CustomTextWatcher textWatcher = new CustomTextWatcher(edList, registrationButton);
        for (EditText editText : edList) editText.addTextChangedListener(textWatcher);


        initDatePicker(getActivity());
        dateButton = view.findViewById(R.id.button_select_birth_day);
        dateButton.setText(getCurrentDate());


        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        registrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.name.setValue(et_name.getText().toString());
                viewModel.surname.setValue(et_surname.getText().toString());
                viewModel.login.setValue(et_login.getText().toString());
                viewModel.password.setValue(et_password.getText().toString());
                viewModel.confPassword.setValue(et_confPassword.getText().toString());

                try {
                    viewModel.registerNewUser();
                } catch (DataNotValidException | UserAlreadyRegisteredException e ) {
                    Toast toast = Toast.makeText(getContext(), e.getMessage(),Toast.LENGTH_LONG);
                    toast.show();
                }

                if(viewModel.isDataValid.getValue())
                    getNavigator().showMainScreen();
            }
        });
        dateButton.setOnClickListener(v -> datePickerDialog.show() );
    }



    private String getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        month = month + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);
        return makeDateString(day, month, year);
    }
    private void initDatePicker(Context context) {
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;

                viewModel.birthDay.setValue(day);
                viewModel.birthMonth.setValue(month);
                viewModel.birthYear.setValue(year);

                String date = makeDateString(day, month, year);
                dateButton.setText(date);
            }
        };

        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        int style = AlertDialog.THEME_HOLO_LIGHT;

        datePickerDialog = new DatePickerDialog(context, style, dateSetListener, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

        viewModel.birthDay.setValue(day);
        viewModel.birthMonth.setValue(month);
        viewModel.birthYear.setValue(year);
    }

    private String makeDateString(int day, int month, int year) {
        return getDayFormat(day) + " " + getMonthFormat(month) + " " + year;
    }
    private String getMonthFormat(int month) {
        if (month == 1) return "JAN";
        if (month == 2) return "FEB";
        if (month == 3) return "MAR";
        if (month == 4) return "APR";
        if (month == 5) return "MAY";
        if (month == 6) return "JUN";
        if (month == 7) return "JUL";
        if (month == 8) return "AUG";
        if (month == 9) return "SEP";
        if (month == 10) return "OCT";
        if (month == 11) return "NOV";
        else return "DEC";
    }
    private String getDayFormat(int day) {
        if( 0 < day && day < 10) return "0" + day;
        else return String.valueOf(day);
    }

    private INavigator getNavigator() {
        INavigator navigator;
        FragmentActivity fragmentActivity = requireActivity();
        if(INavigator.class.isAssignableFrom(fragmentActivity.getClass())) {
            navigator = (INavigator) fragmentActivity;
        } else {
            throw new RuntimeException("FragmentActivity dosen't implement INavigator");
        }
        return navigator;
    }
}
