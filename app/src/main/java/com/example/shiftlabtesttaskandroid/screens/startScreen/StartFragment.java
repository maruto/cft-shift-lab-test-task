package com.example.shiftlabtesttaskandroid.screens.startScreen;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.example.shiftlabtesttaskandroid.App;
import com.example.shiftlabtesttaskandroid.Navigator.INavigator;
import com.example.shiftlabtesttaskandroid.R;
import com.example.shiftlabtesttaskandroid.screens.ViewModelFactory;

public class StartFragment extends Fragment {

    private StartScreenViewModel viewModel;

    private Button showLogScreenBtn;
    private Button showRegScreenBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        viewModel = new ViewModelProvider(this, new ViewModelFactory((App) getActivity().getApplicationContext())).get(StartScreenViewModel.class);

        View view = inflater.inflate(R.layout.fragment_start_screen, container, false);
        showLogScreenBtn = view.findViewById(R.id.button_onLogin);
        showRegScreenBtn = view.findViewById(R.id.button_onRegistration);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        showLogScreenBtn.setOnClickListener(v -> getNavigator().showLoginScreen());
        showRegScreenBtn.setOnClickListener(v -> getNavigator().showRegistrationScreen());

        if(viewModel.isCurrentUserSet.getValue())
            getNavigator().showMainScreen();
    }

    private INavigator getNavigator() {
        INavigator navigator;
        FragmentActivity fragmentActivity = requireActivity();
        if(INavigator.class.isAssignableFrom(fragmentActivity.getClass())) {
            navigator = (INavigator) fragmentActivity;
        } else {
            throw new RuntimeException("FragmentActivity dosen't implement INavigator");
        }
        return navigator;
    }
}