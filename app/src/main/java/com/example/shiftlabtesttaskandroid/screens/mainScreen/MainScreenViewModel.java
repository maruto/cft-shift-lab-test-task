package com.example.shiftlabtesttaskandroid.screens.mainScreen;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.shiftlabtesttaskandroid.model.User;
import com.example.shiftlabtesttaskandroid.model.UserService;

public class MainScreenViewModel extends ViewModel {
    private UserService userService;

    private MutableLiveData<User> _currentUser = new MutableLiveData<>();
    public LiveData<User> currentUser = _currentUser;

    public MainScreenViewModel(UserService userService) {
        this.userService = userService;
        _currentUser.setValue(userService.getCurrentUser());
    }

    public void signOut() {
        userService.signOutUser();
    }
}
