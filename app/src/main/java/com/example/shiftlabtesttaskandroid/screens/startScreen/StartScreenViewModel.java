package com.example.shiftlabtesttaskandroid.screens.startScreen;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.shiftlabtesttaskandroid.model.IUsersListener;
import com.example.shiftlabtesttaskandroid.model.UserService;

public class StartScreenViewModel extends ViewModel implements IUsersListener {
    private UserService userService;

    private final MutableLiveData<Boolean> _isCurrentUserSet = new MutableLiveData<>();
    public LiveData<Boolean> isCurrentUserSet = _isCurrentUserSet;

    public StartScreenViewModel(UserService userService) {
        this.userService = userService;
        userService.addListener(this);
        _isCurrentUserSet.setValue(userService.isCurrentUserSet());
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        userService.removeListener(this);
    }

    @Override
    public void invoke(UserService newUserService) {
        userService = newUserService;
        _isCurrentUserSet.setValue(userService.isCurrentUserSet());
    }
}

