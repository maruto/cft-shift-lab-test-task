package com.example.shiftlabtesttaskandroid.screens.mainScreen;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;

public class UserInfoDialogFragment extends AppCompatDialogFragment {
    private final String userLogin;
    private final String userData;

    public UserInfoDialogFragment(String userLogin, String userData) {
        this.userData = userData;
        this.userLogin= userLogin;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("User: " + userLogin)
                .setMessage(userData)
                .setPositiveButton("close", (dialog, id) -> dialog.cancel());
        return builder.create();
    }
}
