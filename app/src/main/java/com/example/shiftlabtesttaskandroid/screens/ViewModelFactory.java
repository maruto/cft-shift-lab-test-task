package com.example.shiftlabtesttaskandroid.screens;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.shiftlabtesttaskandroid.App;
import com.example.shiftlabtesttaskandroid.model.UserService;
import com.example.shiftlabtesttaskandroid.screens.loginScreen.LoginScreenViewModel;
import com.example.shiftlabtesttaskandroid.screens.mainScreen.MainScreenViewModel;
import com.example.shiftlabtesttaskandroid.screens.registrationScreen.RegistrationScreenViewModel;
import com.example.shiftlabtesttaskandroid.screens.startScreen.StartScreenViewModel;

import org.jetbrains.annotations.NotNull;

public class ViewModelFactory extends ViewModelProvider.AndroidViewModelFactory {
    private final UserService userService;

    public ViewModelFactory(App app) {
        super(app);
        userService = app.userService;
    }

    @NonNull
    @NotNull
    @Override
    public <T extends ViewModel> T create(@NonNull @NotNull Class<T> modelClass) {
        ViewModel viewModel;
        if(modelClass.isAssignableFrom(LoginScreenViewModel.class)) {
            viewModel = new LoginScreenViewModel(userService);
        } else if(modelClass.isAssignableFrom(MainScreenViewModel.class)) {
            viewModel = new MainScreenViewModel(userService);
        } else if(modelClass.isAssignableFrom(RegistrationScreenViewModel.class)) {
            viewModel = new RegistrationScreenViewModel(userService);
        }else if(modelClass.isAssignableFrom(StartScreenViewModel.class)) {
            viewModel = new StartScreenViewModel(userService);
        } else {
            throw new IllegalStateException("Unknown view model class");
        }
        return (T) viewModel;
    }
}
