package com.example.shiftlabtesttaskandroid.screens.loginScreen;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.shiftlabtesttaskandroid.model.UserService;
import com.example.shiftlabtesttaskandroid.model.exceptions.UserNotRegisteredException;
import com.example.shiftlabtesttaskandroid.model.exceptions.WrongPasswordException;

public class LoginScreenViewModel extends ViewModel {

    private UserService userService;
    public MutableLiveData<String> login = new MutableLiveData<>();
    public MutableLiveData<String> password = new MutableLiveData<>();

    private MutableLiveData<Boolean> _isDataValid = new MutableLiveData<>();
    public LiveData<Boolean> isDataValid = _isDataValid;

    public  LoginScreenViewModel(UserService userService) {
        this.userService = userService;
        _isDataValid.setValue(false);
    }

    public void loginUser() throws UserNotRegisteredException, WrongPasswordException {
        _isDataValid.setValue(false);
        userService.loginUser(login.getValue(), password.getValue());
        _isDataValid.setValue(true);
    }
}
